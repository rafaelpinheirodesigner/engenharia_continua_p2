// assets/imagens.js
import thumb1 from './thumb1.webp';
import thumb2 from './thumb2.webp';
import thumb3 from './thumb3.webp';
import thumb4 from './thumb4.webp';
import thumb5 from './thumb5.webp';
import thumb6 from './thumb6.webp';
import thumb7 from './thumb7.webp';
import thumb8 from './thumb8.webp';


export const thumbs = {
  thumb1,
  thumb2,
  thumb3,
  thumb4,
  thumb5,
  thumb6,
  thumb7,
  thumb8,
};
