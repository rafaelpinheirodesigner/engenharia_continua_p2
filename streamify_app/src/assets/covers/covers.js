// assets/imagens.js
import cover1 from './cover1.jpg';
import cover2 from './cover2.jpg';
import cover3 from './cover3.jpg';
import cover4 from './cover4.jpg';
import cover5 from './cover5.jpg';
import cover6 from './cover6.jpg';

export const covers = {
  cover1,
  cover2,
  cover3,
  cover4,
  cover5,
  cover6
};
